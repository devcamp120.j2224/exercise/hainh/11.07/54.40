

package com.example.demo;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class api  {
  
    @CrossOrigin
	@GetMapping("/devcamp-monthday")
	public String getDateViet() {
		DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        return String.format("Hello pizza lover ! hôm nay %s , mua 1 tặng 1.", dtfVietnam.format(today));
	}

    @CrossOrigin
    @GetMapping("/devcamp-number")
    // defaultValue là giá trị mặc định có dòng đó nếu k truyền tham số thì sẽ ra , còn name = "value" , nếu muốn truyền thì fai sửa bên phần FE api
	public String getNumberRamdom(@RequestParam(name = "username" , defaultValue = "haiongngoai") String username) {
        int soNgauNhien1 = ThreadLocalRandom.current().nextInt(1,6);
        return  String.format("xin chào %s !, số ngẫu nhiên của bạn là %s",username ,soNgauNhien1);
	}
}

